class PhotographerView {
  showPhotographerDetails(photographerDetails, photographerMedia, filteredMedia) {
    this.selectOption(filteredMedia);
    let htmlContent = this.createPhotographerView(photographerDetails, photographerMedia, filteredMedia);

    const htmlHeaderInfo = document.querySelector('.photographer_info');
    htmlHeaderInfo.innerHTML = htmlContent.headerInfo;

    const htmlHeaderThumbnail = document.querySelector('.photographer_header_thumbnail');
    htmlHeaderThumbnail.innerHTML = htmlContent.headerThumbnail;

    this.displayMediaCard(htmlContent.mediaCards);

    this.initializeTotalLikesNumber(htmlContent.likesNumberList);

    let lightbox = new Lightbox;
    lightbox.createLightbox(photographerMedia);

    this.animateHeartButtons();
  }

  createPhotographerView(photographer, media, filteredMedia) {
    // Creating header photographer info and image thumbnail:
    let htmlHeaderInfo = `
      <h1>${photographer.name}</h1>
      <p class="location">${photographer.city}, ${photographer.country}</p>
      <p class="tagline">${photographer.tagline}</p>
      <div class="price-info">
        <div class="media-likes">
          <span
            id="total_likes_number"
            aria-atomic="true"
            aria-live="assertive"
            aria-label="Nombre total de likes"
          >
          </span>
          <icon class="heart_black"></icon>
        </div>
        <div aria-label="Prix à la journée">${photographer.price}€ / jour</div>
      </div>
    `;

    let htmlHeaderThumbnail = `
      <img
        alt="${photographer.name}"
        src="assets/photographers/${photographer.portrait}"
      />`;

    // Creating image and video cards (mediaCard) by looping through media data
    // Creating variable to fill in with loop return values

    let htmlLightboxDisplay = [];
    this.toggleFilterOptions();
    let cardObject = this.createHtmlMediaCards(filteredMedia.sortedByPopularity);

    let modalInfo = document.getElementById('contact');
    modalInfo.innerHTML = 'Contactez-moi' + '\n' + photographer.name

    // Returning total HTML result
    return {
      headerInfo: htmlHeaderInfo,
      headerThumbnail: htmlHeaderThumbnail,
      mediaCards: cardObject.htmlMediaCards,
      lightboxDisplay: htmlLightboxDisplay,
      likesNumberList: cardObject.likesNumberList,
    };
  }

  incrementLikesCount(event) {
    let spanCount = event.target.parentNode.querySelector('.likes');
    spanCount.innerHTML = parseInt(spanCount.innerHTML)+1;
    let totalLikesCount = document.getElementById('total_likes_number');
    totalLikesCount.innerHTML = parseInt(totalLikesCount.innerHTML)+1;
  }

  initializeTotalLikesNumber(likesNumberList) {
    let totalLikesNumber = likesNumberList.reduce(
      (accumulator, currentValue) => accumulator + currentValue,
    );
    let totalLikesHtmlDisplay = document.getElementById('total_likes_number');
    totalLikesHtmlDisplay.innerHTML = totalLikesNumber.toString();
  }

  toggleFilterOptions() {
    let filterToggle = document.getElementById('filter_caret');
    let filterSelect = document.querySelector('.select_options');
    let filterOptions = document.querySelectorAll('.select_option');
    filterToggle.addEventListener('click', () => {
      if (!filterToggle.hasAttribute('style')) {
        filterToggle.setAttribute('style', 'rotate:180deg');
        filterSelect.setAttribute('style', 'height:inherit')
        for (let divElement of filterOptions) {
          divElement.setAttribute('style', 'display:block')
        }
      } else {
        filterToggle.removeAttribute('style');
        filterSelect.removeAttribute('style');
        for (let divElement of filterOptions) {
          divElement.removeAttribute('style');
        }
      }
    })
  }

  selectOption(filteredMedia) {
    let filterSelect = document.querySelector('.select_options');
    let filterOptions = document.querySelectorAll('.select_option');
    let filterToggle = document.getElementById('filter_caret');
    let popularity = document.getElementById('popularity');
    let date = document.getElementById('date');
    let title = document.getElementById('title');
    let currentOption = document.getElementById('current_option');

    popularity.addEventListener('click', (event) => {
      currentOption.innerHTML = 'Popularité';
      filterSelect.removeAttribute('style');
      filterToggle.removeAttribute('style');
      for (let divElement of filterOptions) {
        divElement.removeAttribute('style');
      }
      let cardObject = this.createHtmlMediaCards(filteredMedia.sortedByPopularity);
      this.displayMediaCard(cardObject.htmlMediaCards);
      this.animateHeartButtons();
    })
    date.addEventListener('click', () => {
      currentOption.innerHTML = 'Date';
      filterSelect.removeAttribute('style');
      filterToggle.removeAttribute('style');
      for (let divElement of filterOptions) {
        divElement.removeAttribute('style');
      }
      let cardObject = this.createHtmlMediaCards(filteredMedia.sortedByDate);
      this.displayMediaCard(cardObject.htmlMediaCards);
      this.animateHeartButtons();
    })
    title.addEventListener('click', () => {
      currentOption.innerHTML = 'Titre';
      filterSelect.removeAttribute('style');
      filterToggle.removeAttribute('style');
      for (let divElement of filterOptions) {
        divElement.removeAttribute('style');
      }
      let cardObject = this.createHtmlMediaCards(filteredMedia.sortedByTitle);
      this.displayMediaCard(cardObject.htmlMediaCards);
      this.animateHeartButtons();
    })
  }

  createHtmlMediaCards(media) {
    let htmlMediaCards = [];
    let likesNumberList = [];

    for (let element of media) {
      // Declaring a variable to discriminate between video and image elements
      let mediaHtml = Factory.generateMediaTagFactory(element, '');

      // Creating the html for the media cards
      let htmlMediaCard = `
        <figure class="image-slot">
          <div
            aria-label="${element.title}, closeup view"
            class="photographer-image" 
            role="button"
            tabindex="0"
          >
            ${mediaHtml}
          </div>
          <figcaption class="media-caption">
            <div class="media-title">${element.title}</div>
            <div class="media-likes">
              <span id="likes_number_${element.id}" data-likes-id="${element.id}" class="likes">${element.likes}</span>
              <button class="heart" id="heart_${element.id}" aria-label="Bouton j'aime"></button>
            </div>
          </figcaption>
        </figure>
      `
      // Storing the media elements in a table
      htmlMediaCards.push(htmlMediaCard);
      likesNumberList.push(element.likes);
    }
    return {
      htmlMediaCards : htmlMediaCards,
      likesNumberList : likesNumberList,
    };
  }

  displayMediaCard(mediaCards) {
    let mediaCard = '';

    for (let element of mediaCards) {
      mediaCard += element;
    }

    const htmlMediaCards = document.querySelector('.photographer_art_pieces');
    htmlMediaCards.innerHTML = mediaCard;
  }

  animateHeartButtons() {
    let mediaCardHearts = document.querySelectorAll('.heart');
    console.log(mediaCardHearts);

    for (let heartElement of mediaCardHearts) {
      heartElement.addEventListener('click', (event) => {
        this.incrementLikesCount(event);
      });
    }
  }
}
